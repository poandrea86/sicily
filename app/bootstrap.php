<?php
session_start();
use DI\ContainerBuilder;
use function DI\autowire;
use App\Providers\Container;
require __DIR__ . '/../vendor/autoload.php';
$containerBuilder = new ContainerBuilder();
$dynamicClass = [];
$files = glob(__DIR__ . "/{*/}*.php", GLOB_BRACE);
foreach ($files as $file) {
    $name = preg_replace("/\.php/", "", basename($file));
    $ns = null;
    $handle = fopen($file, "r");
    if ($handle) {
        while (($line = fgets($handle)) !== false) {
            if (strpos($line, 'namespace') === 0) {
                $parts = explode(' ', $line);
                $ns = rtrim(trim($parts[1]), ';');
                break;
            }
        }
        fclose($handle);
    }
    $dynamicClass[$name] = autowire("\\" . $ns . "\\" . $name);
}
$containerBuilder->addDefinitions($dynamicClass);
$container = $containerBuilder->build();
Container::setInstance($container);
return $container;
