<?php
/**
 * Created by PhpStorm.
 * User: andrea
 * Date: 23/10/2019
 * Time: 21:53
 */

namespace App\Providers;

use FastRoute;

class Router
{
    public function start()
    {
        $container = new Container();
        $dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {
            $routes = (array)$this->getRoutes();
            foreach ($routes as $method => $route) {
                switch ($method) {
                    case 'GET':
                        foreach ($route as $url => $parameters) {
                            $r->addRoute('GET', $url, [$parameters[0], $parameters[1]]);
                        }
                        break;
                    case 'POST':
                        foreach ($route as $url => $parameters) {
                            $r->addRoute('POST', $url, [$parameters[0], $parameters[1]]);
                        }
                        break;
                }
            }
        });

        $httpMethod = $_SERVER['REQUEST_METHOD'];
        $uri = $_SERVER['REQUEST_URI'];
        if (false !== $pos = strpos($uri,    '?')) {
            $uri = substr($uri, 0, $pos);
        }
        $uri = rawurldecode($uri);
        $routeInfo = $dispatcher->dispatch($httpMethod, $uri);
        switch ($routeInfo[0]) {
            case FastRoute\Dispatcher::NOT_FOUND:
                echo json_encode(['Not found 404']);
                break;
            case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
                echo json_encode(['Bad Method']);
                break;
            case FastRoute\Dispatcher::FOUND:
                Config::getSessionId();
                $instance = $container->getInstance();
                $instance->call($routeInfo[1], $routeInfo[2]);
                break;
        }

    }

    private function getRoutes() {
        return [
            'GET' => [
                '/' => ['HomeController', 'index'],
                '/index.php' => ['HomeController', 'index'],
            ],
            'POST' => [
                '/ajax/search' => ['AjaxController', 'index'],
            ]
        ];
    }
}
