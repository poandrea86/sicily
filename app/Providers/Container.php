<?php
namespace App\Providers;

class Container
{
    private static $instance;
    public static function service($serviceName) {
        return self::$instance->get($serviceName);
    }
    public static function getInstance() {
        return self::$instance;
    }
    public static function setInstance($container) {
        return self::$instance = $container;
    }
}
