<?php
namespace App\Providers;

use Monolog\Logger as MonologLogger;
use Monolog\Handler\StreamHandler;

class Logger
{
    private $logger;
    private $config;
    public function __construct(Config $config)
    {
        $this->config = $config;
        $logger = $this->withStdOutput();
        $this->logger = $logger;
    }
    private function withStdOutput()
    {
        $logger = new MonologLogger('logger-app');
        $logger->pushHandler(new StreamHandler($this->config->getValue('PHP_ERROR_LOG'), MonologLogger::DEBUG));
        return $logger;
    }
    public function debug($message, $context = [])
    {
        $this->logger->debug($message, $context);
    }
    public function     info($message, $context = [])
    {
        $this->logger->info($message, $context);
    }
    public function notice($message, $context = [])
    {
        $this->logger->notice($message, $context);
    }
    public function warning($message, $context = [])
    {
        $this->logger->warning($message, $context);
    }
    public function error($message, $context = [])
    {
        $this->logger->error($message, $context);
    }
    public function critical($message, $context = [])
    {
        $this->logger->critical($message, $context);
    }
    public function alert($message, $context)
    {
        $this->logger->alert($message, $context);
    }
    public function emergency($message, $context)
    {
        $this->logger->emergency($message, $context);
    }
}
