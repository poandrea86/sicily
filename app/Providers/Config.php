<?php

namespace App\Providers;

use Dotenv\Dotenv;

class Config
{
    private static $sessionId;

    public function __construct()
    {
        $dirEnv = __DIR__ . '/../../../';
        if (file_exists($dirEnv . '.env')) {
            $dotEnv = Dotenv::create($dirEnv);
            $dotEnv->load();
        }
        $this->setSessionId();
    }

    public static function getValue($key)
    {
        $value = getenv($key);
        if (!empty($value)) {
            return $value;
        }
    }

    public function setSessionId() {
        Config::$sessionId = session_id();
    }

    public static function getSessionId() {
        return Config::$sessionId;
    }
}
