<?php

namespace App\Providers;

use Exception;

class View
{
    private $config;
    private $logger;

    public function __construct(Config $config, Logger $logger)
    {
        $this->config = $config;
        $this->logger = $logger;
    }

    public function render($name, $variables = [], $get = false)
    {
        try {
            ob_start();
            // Assign variables to global context inside $this
            array_walk($variables, function ($item, $key) {
                $this->{$key} = $item;
            });
            $templateFile = $this->template() . "/{$name}.php";
            if (file_exists($templateFile)) {
                require_once $templateFile;
            } else {
                throw new Exception("Template {$templateFile} not found");
            }
            $html = ob_get_contents();
            ob_end_clean();
            if ($get) {
                return $html;
            }
            echo $html;
        } catch (Exception $e) {
            $this->logger->info($e->getMessage() . " at line " . $e->getLine() . " in file " . $e->getFile() );
        }
    }

    public function assets() {
        return Config::getValue('SITE_HOST') . "/assets";
    }

    private function template() {
        return __DIR__ . '/../../assets/template';
    }
}
