<?php
namespace App\Controllers;

use App\Providers\View;

class HomeController {

    private $view;

    public function __construct(View $view)
    {
        $this->view = $view;
    }

    public function index() {
        $this->view->render('index');
    }
}
