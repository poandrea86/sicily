<?php
require_once __DIR__  . "/app/bootstrap.php";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="it" class="no-js">
<?php
use App\Providers\Container;
$router = Container::service('Router');
$router->start();
?>
</html>

